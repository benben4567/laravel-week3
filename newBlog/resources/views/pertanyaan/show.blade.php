@extends('template.master')

@section('content')
<div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="{{asset ('adminLTE/dist/img/cogan.jpg')}}" alt="User Image">
                  <span class="username"><a href="#">{{$question->penanya->full_name}}</a></span>
                  <span class="description">{{ $question->created_at }}</span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                    <i class="far fa-circle"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- post text -->
                <h6><strong>Title: </strong> {{ $question->title}} </h6>
                <p><strong>Question: </strong> {{ $question->content }}</p>

                <!-- Attachment -->
              

                <!-- Social sharing buttons -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i> Share</button>
                <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                <span class="float-right text-muted">45 likes - 2 comments</span>
              </div>
              <!-- /.card-body -->
              <div class="card-footer card-comments" style="display:block;">
              @foreach($question->answer as $jawaban)
              <div class="card-comment">


                      <!-- User image -->
                      <img class="img-circle img-sm" src="{{asset('/adminLTE/dist/img/'. $jawaban->profile->photo)}}" alt="User Image">
                      <div class="comment-text">
                    <span class="username">
                    {{$jawaban->profile->full_name}}
                    <span class="text-muted float-right">{{$jawaban->created_at}}</span>
                    </span>
                    {{$jawaban->content}}
                    </div>
              </div>
              @endforeach
              </div>
              <!-- /.card-footer -->
              <div class="card-footer">
                <form action="/answer/{{$question->id}}" method="post">
                    @csrf
                  <img class="img-fluid img-circle img-sm" src="{{asset('/adminLTE/dist/img/'. $question->penanya->photo)}}" alt="Alt Text">
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push">
                    <input type="text" class="form-control form-control-sm" name="content" placeholder="Press enter to post comment">
                  </div>
                  <input type="submit" value="answer" class="btn btn-sm btn-primary mt-2">
                </form> 
              </div>
              <!-- /.card-footer -->
            </div>

@endsection