<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $table = "questions";
    //protected $fillable = ["title", "content"];
    protected $guarded =[];

    public function penanya(){
        return $this->belongsTo('App\Profile','profile_id');
    }

    public function answer(){
        return $this->hasMany('App\Answer','question_id');
    }
    
}
