<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/data-table', function () {
    //return view('data-table');
//});

//Route::get('/pertanyaan/create', 'PertanyaanController@create');
//Route::post('/pertanyaan', 	'PertanyaanController@store');
//Route::get('/pertanyaan', 'PertanyaanController@index')->name('pertanyaan.index');
//Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
//Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
//Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
//Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');

//Route::resource('question', 'QuestionController');
Route::resource('pertanyaan', 'PertanyaanController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/answer/{pertanyaan_id}','AnswerController@store');
